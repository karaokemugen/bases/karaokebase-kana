# Kana Base

Karaoke Mugen song database for Kana/Kanji karaokes

This repository require karaokebase to work. It's more like an addon than a base itself.

To add it, you need to :
- clone this project
- download manually theirs medias by ftp
- add a local repository named "karaokebase-kana" in Karaoke Mugen application with the karaokes folder and the medias folder

This base is deprecated, it has been merged into https://gitlab.com/karaokemugen/bases/karaokebase
